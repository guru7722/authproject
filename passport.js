const LocalStrategy = require("passport-local").Strategy;
const bcrypt = require("bcrypt");
const client = require("./db");

function initialize(passport) {
  passport.use(
    new LocalStrategy({usernameField:'username',passwordField:'password'},async (username, password, done) => {
      try {
        const find_email = await client.query("SELECT * FROM users WHERE email =$1", [username]);

        if (find_email.rows.length <= 0) {
          return done(null, false, { message: "user do not exist" });
        } else {
          bcrypt.compare(password, find_email.rows[0].password, (err, matched) => {
            if (matched) {
              return done(null, find_email.rows[0]);
            } else {
              return done(null, false, { message: "wrong password" });
            }
          });
        }
      } catch (err) {
        return done(err);
      }
    })
  );
  passport.serializeUser((user, done) => done(null, user.user_id));

  passport.deserializeUser((id, done) => {
    client.query("SELECT * FROM users WHERE user_id = $1", [parseInt(id)], (err, results) => {
      console.log(id)
      if (err) {
       throw err;
      }

     return done(null, results.rows[0]);
    });
  });
}
module.exports = initialize;
