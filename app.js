require("dotenv").config();
const express = require("express");
const bodyparser = require("body-parser");
const bcrypt = require("bcrypt");
const client = require("./db");
const session = require("express-session");
const flash = require("express-flash");
const passport = require("passport");

const localStr=require('./passport');
localStr(passport);

const port = 3000;
const app = express();

//middleware
app.use(express.json());
app.set("view engine", "ejs");

app.use(bodyparser.urlencoded({ extended: true }));
app.use(bodyparser.json());
app.use(express.json());
app.use(flash());
app.use(
  session({
    secret: process.env.SECRET_MSG,
    resave: false,
    saveUninitialized: false,
  })
);

app.use(passport.initialize());
app.use(passport.session());
//-----------------

//render all the html (ejs tamplates)
app.get("/",checkUserIsauthanticated, (req, res) => {
  res.render("home.ejs");
})

app.get("/login",checkUserIsauthanticated, (req, res) => {
  res.render("login.ejs");
});
app.get("/register",checkUserIsauthanticated, (req, res) => {
  res.render("register.ejs");
});
app.get("/secrets", (req, res) => {
  res.render("secrets.ejs");
});
//handling post req for registrations----------------------------------------------------------------------------------

app.post("/register", async (req, res) => {
  //fetching data from client req
  const user = {
    email: req.body.username,
    password: req.body.password,
  };

  try {
    const Email_from_db = await client.query("SELECT * FROM users WHERE email =$1", [user.email]);
    //user do not exist
    if (Email_from_db.rows <= 0 && Email_from_db.rows.length <= 0) {
      //creating new user
      bcrypt.hash(user.password, parseInt(10), async (err, hasdpass) => {
        await client.query("INSERT INTO USERS( email,password) VALUES($1,$2)", [user.email, String(hasdpass)]);
        console.log("user successfully registered");
        res.redirect("/secrets");
      });
      // if user do exist in database
    } else {
      console.log("userAlready exist");
    }
  } catch (error) {
    console.log("first error");
  }
});
//------------------------local strategy-----------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------------------
/*login post handling*/
app.post('/login', 
  passport.authenticate('local', { failureRedirect: '/login' }),
  function(req, res) {
    res.redirect('/secrets');
  });

 //logout will remove the session and redirect to the login page 
app.get('/logout',(req,res)=>{
  req.logOut(()=>{
    res.redirect('/login')
  });
  
})

app.get('/',)
//-------------------------------------------------------------------------------------------------------------------
function checkUserIsauthanticated(req,res,next){
  if(req.isAuthenticated())
  {
    return res.redirect('/secrets');
  }
  next()
}
function checkUserIsNotauthanticated(req,res,next){
  if(req.isAuthenticated()){
    next()
  }
  return res.redirect('/login')
}

app.listen(port, () => {
  console.log("server is up on port " + port);
});
