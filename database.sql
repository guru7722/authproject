--create data base
create database ejsauth;

create table users(
    user_id SERIAL PRIMARY KEY,
    email VARCHAR(130) NOT NULL,
    password VARCHAR(250) NOT NULL
);